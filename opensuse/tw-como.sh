#!/bin/bash

set -ex
set -o pipefail

# Plan:
# - clone FDBuild
# - create KWinFT template
# - replace config file to enforce Qt6 build
# - cleanup tooling and build files.

zypper install --no-confirm wlroots-devel

python3 -m venv /opt/venv
source /opt/venv/bin/activate
pip3 install fdbuild

mkdir -p /opt/kwinft-build && cd /opt/kwinft-build

fdbuild --init-with-template kwinft

cd kwinft

printf "build:
  plugin: ninja
  threads: max
configure:
  plugin: cmake
  options:
    - QT_MAJOR_VERSION=6
    - BUILD_WITH_QT6=ON
install:
  path: /usr
projects:
  - wrapland
  - kwinft
  - disman
  - kdisplay
source:
  plugin: git
  branch: master
  depth: 1\n" > fdbuild.yaml

fdbuild --noconfirm

# venv deactivate
deactivate

cd /opt
rm -rf /opt/kwinft-build
