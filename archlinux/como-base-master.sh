#!/bin/bash

set -ex
set -o pipefail

# Plan:
# - structure all dependencies of KWin by selecting KWin
# - exclude KDE Frameworks again (already included in the image)
# - exclude KWin
# - build all remaining dependencies (part of kde/workspace/)
# - cleanup tooling and build files.

python -m venv /opt/venv
source /opt/venv/bin/activate
pip install fdbuild

mkdir -p /opt/kde-build && cd /opt/kde-build

printf "install:
  path: /usr
configure:
  plugin: cmake
  options:
    - BUILD_TESTING=OFF
    - QT_MAJOR_VERSION=6
    - BUILD_WITH_QT6=ON
    - BUILD_QT5=OFF
build:
  plugin: ninja
  threads: max
source:
  plugin: git
  branch: master
  depth: 1
structure:
  enabled: true
  plugin: kde
  branch-group: kf6-qt6
  flat: false
  selection:
  - kwin\n" > fdbuild.yaml

fdbuild --noconfirm --only-structure

cat fdbuild.yaml
cat kde/workspace/fdbuild.yaml

# Exclude KDE Frameworks again and KWin. Disable also the structure plugin
# so we don't rerun it again.
sed '/- frameworks/d' fdbuild.yaml > temp_fdbuild.yaml
sed 's/enabled\: true/enabled\: false/g' temp_fdbuild.yaml > fdbuild.yaml

sed '/- kwin/d' kde/workspace/fdbuild.yaml > temp_fdbuild.yaml
mv temp_fdbuild.yaml kde/workspace/fdbuild.yaml

cat fdbuild.yaml
cat kde/workspace/fdbuild.yaml

fdbuild --noconfirm

# venv deactivate
deactivate

cd /opt
rm -rf /opt/kde-build
rm -rf /opt/venv
