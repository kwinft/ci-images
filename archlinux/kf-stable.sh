#!/bin/bash

set -ex
set -o pipefail

# Plan:
# - install FDBuild
# - build all KDE Frameworks from last tag
# - cleanup tooling and build files

python -m venv /opt/venv
source /opt/venv/bin/activate
pip install fdbuild

mkdir -p /opt/kde-build
cd /opt/kde-build

printf "install:
  path: /usr
configure:
  plugin: cmake
  options:
    - BUILD_TESTING=OFF
    - QT_MAJOR_VERSION=6
    - BUILD_WITH_QT6=ON
    - PHONON_BUILD_QT5=OFF
build:
  plugin: ninja
  threads: max
source:
  plugin: git
  branch: master
  hooks:
    post: bash -c 'cd source && git checkout \$(git describe --abbrev=0 \$(git rev-list --tags --max-count=1)) && cd ..'
structure:
  enabled: true
  plugin: kde
  branch-group: kf6-qt6
  flat: true
  selection:
  - frameworks\n" > fdbuild.yaml

fdbuild --noconfirm --only-structure

# Disalbe kapidox. It does not contain a valid CMake file and build fails.
# TODO(romangg): remove once FDBuild handles this correctly.
sed -i 's/- kapidox/#- kapidox/g' fdbuild.yaml
sed -i 's/- kdewebkit/#- kdewebkit/g' fdbuild.yaml
sed -i 's/enabled\: true/enabled\: false/g' fdbuild.yaml

branch_master () {
echo "  branch: master" >> $1/fdbuild.yaml
}

branch_master phonon
branch_master plasma-wayland-protocols
branch_master polkit-qt-1
branch_master qca

fdbuild --noconfirm

# venv deactivate
deactivate

cd /opt
rm -rf /opt/kde-build
rm -rf /opt/venv
