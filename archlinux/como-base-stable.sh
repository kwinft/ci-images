#!/bin/bash

set -eux
set -o pipefail

# Plan:
# - structure all dependencies of KWin by selecting KWin
# - exclude KDE Frameworks again (already included in the image)
# - exclude KWin
# - build all remaining dependencies (part of kde/workspace/)
# - build KWinFT
# - cleanup tooling and build files.

python -m venv /opt/venv
source /opt/venv/bin/activate
pip install fdbuild

mkdir -p /opt/kde-build && cd /opt/kde-build

printf "install:
  path: /usr
configure:
  plugin: cmake
  options:
    - BUILD_TESTING=OFF
    - QT_MAJOR_VERSION=6
    - BUILD_WITH_QT6=ON
    - BUILD_QT5=OFF
build:
  plugin: ninja
  threads: max
source:
  plugin: git
  branch: 'Plasma/%s'
structure:
  enabled: true
  plugin: kde
  branch-group: kf6-qt6
  flat: false
  selection:
  - kwin\n" "$PLASMA_VERSION" > fdbuild.yaml

fdbuild --noconfirm --only-structure

# kdesupport (polkit-qt-1, phonon) and extragear (libqaccessibilityclient) packages are not
# regularly released with Plasma and need to be built from master.
printf "source:
  branch: master\n" >> kdesupport/fdbuild.yaml
printf "source:
  branch: master\n" >> extragear/fdbuild.yaml

# Exclude KDE Frameworks again and KWin. Disable also the structure plugin
# so we don't rerun it again.
sed '/- frameworks/d' fdbuild.yaml > temp_fdbuild.yaml
sed 's/enabled\: true/enabled\: false/g' temp_fdbuild.yaml > fdbuild.yaml

sed '/- kwin/d' kde/workspace/fdbuild.yaml > temp_fdbuild.yaml
mv temp_fdbuild.yaml kde/workspace/fdbuild.yaml

fdbuild --noconfirm

# venv deactivate
deactivate

cd /opt
rm -rf /opt/kde-build
rm -rf /opt/venv
